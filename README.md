# MyNewt on the 1Bitsy

A port of the [Olimex STM32-E407](https://mynewt.apache.org/latest/os/tutorials/olimex/) BSP for the [1Bitsy](http://1bitsy.org/), including a script for properly flashing an image using the [BlackMagic probe](https://github.com/blacksphere/blackmagic).

## Installation Instructions

Create a new project, as you would [here](https://mynewt.apache.org/latest/os/tutorials/repo/add_repos/). In `project.yml`, add `- 1bitsy-mynewt` under repositories and add the following to the bottom of the file (You'll need version >= 1.3.0 for this part, since we're accessing a gitlab and not a github repo):

```yml
repository.1bitsy-mynewt:
    type: git
    vers: 0.0.1
    url: https://gitlab.com/cellu_cc/1bitsy-mynewt.git
```

Then run `newt install` to install the two repos.

To create a target that points to these two repositories, follow the instructions [here](https://mynewt.apache.org/latest/os/tutorials/olimex/) but use `@1bitsy-mynewt/hw/bsp/1bitsy` as the value for `bsp` instead of the value that is there normally.

## Notes

The use of the `blackmagic.sh` shell script complicates things, since newt doesn't explicitly pass a path to the top level `bsp` directory. Maybe there is a workaround for this.

