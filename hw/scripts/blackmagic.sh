# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.

. $CORE_PATH/hw/scripts/common.sh

#JLINK_GDB_SERVER=JLinkGDBServer

#
# FILE_NAME is the file to load
# FLASH_OFFSET is location in the flash
# BLACKMAGIC_DEV is what we tell GDB this device to be
#

#
# NB: We'll be disabiling windows detect until I can 
# debug on windows (someday)
#

blackmagic_load () {
    GDB_CMD_FILE=.gdb_cmds
    GDB_OUT_FILE=.gdb_out
    PROBES=($(ls -d /dev/cu.usbmodem*1))

    #windows_detect
    #if [ $WINDOWS -eq 1 ]; then
	#JLINK_GDB_SERVER=JLinkGDBServerCL
    #fi

    #check if the $FILE_NAME variable exists
    if [ -z $FILE_NAME ]; then
        echo "Missing filename"
        exit 1
    fi

    #check if that $FILE_NAME points to a real file
    if [ ! -f "$FILE_NAME" ]; then
        #tries stripping current path for readability
        FILE=${FILE_NAME##$(pwd)/}
        echo "Cannot find file" $FILE
        exit 1
    fi
    
    #check if the flash_offset variable is set
    #if [ -z $FLASH_OFFSET ]; then
    #    echo "Missing flash offset"
    #    exit 1
    #fi

    echo "Downloading" $FILE_NAME "to" $FLASH_OFFSET

    echo "target extended-remote ${PROBES[0]}" >> $GDB_CMD_FILE
    #we might need to make the monitor...attach procedure more robust
    echo "monitor swdp_scan" >> $GDB_CMD_FILE
    echo "attach 1" >> $GDB_CMD_FILE
    #echo "set mem inaccessible-by-default off" >> $GDB_CMD_FILE
    echo "load $FILE_NAME" >> $GDB_CMD_FILE
    #echo "restore $FILE_NAME binary $FLASH_OFFSET" >> $GDB_CMD_FILE
    echo "info reg" >> $GDB_CMD_FILE
    echo "quit" >> $GDB_CMD_FILE

    msgs=`arm-none-eabi-gdb -x $GDB_CMD_FILE 2>&1`
    echo $msgs > $GDB_OUT_FILE

    rm $GDB_CMD_FILE

    # Echo output from script run, so newt can show it if things go wrong.
    echo $msgs

    error=`echo $msgs | grep error`
    if [ -n "$error" ]; then
	exit 1
    fi

    error=`echo $msgs | grep -i failed`
    if [ -n "$error" ]; then
	exit 1
    fi

    error=`echo $msgs | grep -i "unknown / supported"`
    if [ -n "$error" ]; then
	exit 1
    fi

    error=`echo $msgs | grep -i "not found"`
    if [ -n "$error" ]; then
	exit 1
    fi

    return 0
}

#
# FILE_NAME is the file to debug
# JLINK_DEV is what we tell JLinkGDBServer this device to be
# EXTRA_GDB_CMDS is for extra commands to pass to gdb
# RESET is set if we should reset the target at attach time
#
blackmagic_debug() {
    #windows_detect
    #if [ $WINDOWS -eq 1 ]; then
	#JLINK_GDB_SERVER=JLinkGDBServerCL
    #fi

    GDB_CMD_FILE=.gdb_cmds
    PROBES=($(ls -d /dev/cu.usbmodem*1))


    if [ -z $FILE_NAME ]; then
        echo "Missing filename"
        exit 1
    fi
    if [ ! -f "$FILE_NAME" ]; then
        echo "Cannot find file" $FILE_NAME
        exit 1
    fi

    echo "Debugging" $FILE_NAME

    echo "target extended-remote ${PROBES[0]}" >> $GDB_CMD_FILE
    #we might need to make the monitor...attach procedure more robust
    echo "monitor swdp_scan" >> $GDB_CMD_FILE
    echo "attach 1" >> $GDB_CMD_FILE
    echo "set mem inaccessible-by-default off" >> $GDB_CMD_FILE
    echo "file $FILE_NAME"

    # Whether target should be reset or not
    if [ ! -z "$RESET" ]; then
        echo "mon reset" >> $GDB_CMD_FILE
        echo "si" >> $GDB_CMD_FILE
    fi

    echo "$EXTRA_GDB_CMDS" >> $GDB_CMD_FILE

	if [ $WINDOWS -eq 1 ]; then
	    FILE_NAME=`echo $FILE_NAME | sed 's/\//\\\\/g'`
	    $COMSPEC /C "start $COMSPEC /C arm-none-eabi-gdb -x $GDB_CMD_FILE $FILE_NAME"
	else
            arm-none-eabi-gdb -x $GDB_CMD_FILE $FILE_NAME
            rm $GDB_CMD_FILE
	fi

    ret
}
