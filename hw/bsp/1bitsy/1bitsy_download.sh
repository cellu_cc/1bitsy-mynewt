#!/bin/sh

echo $BSP_PATH

. $BSP_PATH/hw/scripts/blackmagic.sh

if [ "$MFG_IMAGE" ]; then
    FLASH_OFFSET=0x08000000
elif [ "$BOOT_LOADER" ]; then
    FILE_NAME=$BIN_BASENAME.elf
else
    FLASH_OFFSET=0x08020000
    FILE_NAME=$BIN_BASENAME.srec
    arm-none-eabi-objcopy --adjust-vma=$FLASH_OFFSET -O srec -I binary $BIN_BASENAME.img $BIN_BASENAME.srec
fi

blackmagic_load
#blackmagic_reset_run